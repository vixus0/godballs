extends SamplePlayer

var i = 0

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func footstep():
	i = (i+1) % 2 + 1
	set_default_volume(1.0 - rand_range(0, 0.5))
	set_default_pitch_scale(1.0 + rand_range(0, 0.3))
	play("step" + str(i))