extends KinematicBody2D

const REFLECT_PLAYER_VEL = 10.0
const REFLECT_SPEED_INCR = 40.0

var dir = Vector2()
var dead = false
var spd = 0

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	get_node("../animations").play("glow")
	get_node("AnimatedSprite").set_hidden(true)
	get_node("Sprite").set_hidden(false)
	fire_sound()
	set_fixed_process(true)
	
func target(direction):
	dir = direction
	
func set_speed(speed):
	spd = speed

func _fixed_process(delta):
	if dead:
		return
	
	move(dir * spd * delta)
	
	if is_colliding():
		var collider = get_collider()
		var parent = collider.get_parent()
		
		if collider.get_name() == "level_tiles":
			level_sound()
			bang()
		elif collider.get_name() == "KinematicBody2D":
			if parent.is_in_group("beams"):
				level_sound()
				collider.bang()
				bang()
			elif parent.get_name() == "player":
				if collider.is_attacking():
					reflect_sound()
					set_collision_mask_bit(1,  true)
					dir = -dir
					dir.y += collider.velocity.y * delta * delta * REFLECT_PLAYER_VEL
					spd += REFLECT_SPEED_INCR
					set_rot((-dir).angle())
				else:
					player_sound()
					collider._on_damage()
					bang()
			elif parent.get_name() == "god":
				god_sound()
				collider._on_damage()
				bang()
				
func fire_sound():
	get_node("../SamplePlayer2D").play("fire" + str(randi() % 3))
	
func level_sound():
	get_node("../SamplePlayer2D").play("blip")
	
func reflect_sound():
	get_node("../SamplePlayer2D").play("reflect")

func player_sound():
	get_node("../SamplePlayer2D").play("player_hurt" + str(randi() % 2))
	
func god_sound():
	get_node("../SamplePlayer2D").play("god_hurt" + str(randi() % 3))

func bang():
	dead = true
	get_node("../animations").play("bang")

func destroy():
	get_parent().queue_free()