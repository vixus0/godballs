extends KinematicBody2D

signal god_hurt

const BEAM_SPAWN_DISTANCE = 120
const BEAM_SPEED = 200.0
const BEAM_SPEED_VARIATION = 20.0

onready var beam_scn = preload("res://beam.tscn")

var player_pos = Vector2()
var phase = 0

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process(true)
	get_node("../god_animations").play("idle0")
	
func _process(delta):
	var iris_angle = get_angle_to(player_pos)
	get_node("iris_bg").set_rot(iris_angle)
	
func on_player_pos_update(pos):
	player_pos = pos
	
func create_beam():
	var beam = beam_scn.instance()
	var god_pos = get_global_pos()
	var player_dir = (player_pos - god_pos).normalized()
	var beam_spawn = god_pos + player_dir * BEAM_SPAWN_DISTANCE
	var speed_var = 0 if phase == 0 else rand_range(0, BEAM_SPEED_VARIATION)
	var speed = BEAM_SPEED + speed_var
	beam.set_global_pos(beam_spawn)
	beam.get_node("KinematicBody2D").set_rot(player_dir.angle())
	beam.get_node("KinematicBody2D").set_speed(speed)
	beam.get_node("KinematicBody2D").target(player_dir)
	get_node("../beam_animations").play("fire_beam")
	return beam

func _on_god_animations_finished():
	get_node("../god_animations").play("idle" + str(phase))
	
func _on_damage():
	print('GOD: OUCH!')
	var sample_player = get_node("../SamplePlayer2D")
	sample_player.play("god_scream0")
	get_node("../god_animations").play("hurt")
	emit_signal("god_hurt")
	
func set_phase(ph):
	phase = ph