extends Sprite

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var paused = false
var still_pressed = false

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_opacity(0)
	set_process(true)
	
func _process(delta):
	if Input.is_action_pressed("ui_pause") and not still_pressed:
		if paused:
			unpause()
		else:
			pause()
	still_pressed = Input.is_action_pressed("ui_pause")
	
func pause():
	set_opacity(0.8)
	get_tree().set_pause(true)
	paused = true
	for track in get_node("../../music").get_children():
		track.set_paused(true)
	
func unpause():
	set_opacity(0)
	get_tree().set_pause(false)
	paused = false
	for track in get_node("../../music").get_children():
		track.set_paused(false)