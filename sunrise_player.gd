extends AnimationPlayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	play("init")

func _on_sunrise0_enter_screen():
	play("init")

func _on_sunrise1_enter_screen():
	play("sunrise1")


func _on_sunrise2_enter_screen():
	play("sunrise2")


func _on_sunrise3_enter_screen():
	play("sunrise3")


func _on_sunrise4_enter_screen():
	play("sunrise4")
