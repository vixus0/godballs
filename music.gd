extends SamplePlayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	play("theClimbA")
	play("theClimbB")
	play("theClimbC")
	play("theClimbD")
	set_volume(0, 1)
	set_volume(1, 0)
	set_volume(2, 0)
	set_volume(3, 0)
