extends Node

const BEAM_FIRE_CHANCE_PHASE1 = 0.7
const BEAM_FIRE_CHANCE_PHASE2 = 0.5
const BEAM_DAMAGE = 10
const REFLECTED_BEAM_DAMAGE = 5
const PLAYER_MAX_HP = 100
const GOD_MAX_HP = 100
const PLAYER_BAR_SECTIONS = 10
const GOD_BAR_SECTIONS = 20
const GOD_PHASE2_THRESHOLD = 10

var god_phase = 0
var current_location = 0
var player_hp = PLAYER_MAX_HP
var god_hp = GOD_MAX_HP
var victory = false

onready var player_init_pos = get_node("player_start").get_global_pos()
onready var player_restart_pos = get_node("player_restart").get_global_pos()
onready var god_init_pos = get_node("god_start").get_global_pos()

var touch_move = false

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	get_node("player/KinematicBody2D").connect("pos_update", get_node("god/KinematicBody2D"), "on_player_pos_update")
	get_node("player/KinematicBody2D").connect("player_hurt", self, "_player_hurt")
	get_node("god/KinematicBody2D").connect("god_hurt", self, "_god_hurt")
	set_process_input(true)
	reset_game()
	
func _input(event):
	if Input.is_action_pressed("ui_quit"):
		get_tree().quit()
		
	if victory and event.type in [InputEvent.KEY, InputEvent.SCREEN_TOUCH]:
		get_tree().set_pause(true)
		reset_game()
		
	if event.type == InputEvent.SCREEN_DRAG:
		# only if we're touching
		if touch_move:
			if event.pos.x > 0 and event.pos.x < 150:
				if not Input.is_action_pressed("touch_left"):
					Input.action_press("touch_left")
					Input.action_release("touch_right")
			elif event.pos.x > 150 and event.pos.x < 300:
				if not Input.is_action_pressed("touch_right"):
					Input.action_press("touch_right")
					Input.action_release("touch_left")
		else:
			print('should never happen')
			# stop actions
			if Input.is_action_pressed("touch_left"):
				Input.action_release("touch_left")
			if Input.is_action_pressed("touch_right"):
				Input.action_release("touch_right")
	
	if event.type == InputEvent.SCREEN_TOUCH:
		if event.pressed:
			# only do something if we weren't touching previously
			if event.pos.x > 0 and event.pos.x < 150:
				if not Input.is_action_pressed("touch_left"):
					Input.action_press("touch_left")
					Input.action_release("touch_right")
			elif event.pos.x > 150 and event.pos.x < 300:
				if not Input.is_action_pressed("touch_right"):
					Input.action_press("touch_right")
					Input.action_release("touch_left")
		else:
			# stop actions
			if Input.is_action_pressed("touch_left"):
				Input.action_release("touch_left")
			if Input.is_action_pressed("touch_right"):
				Input.action_release("touch_right")
		touch_move = event.pressed

func _player_hurt():
	player_hp -= BEAM_DAMAGE
	update_player_bar()
	if player_hp == 0:
		# GAME OVER RESTART BOSS
		player_died()
	
func _god_hurt():
	god_hp -= REFLECTED_BEAM_DAMAGE
	update_god_bar()
	if god_hp < GOD_PHASE2_THRESHOLD and god_phase == 0:
		get_node("boss_player").play("boss_next_phase")
	elif god_hp == 0:
		# GAME OVER YOU WIN
		get_node("boss_player").stop_all()
		for child in get_node("beams").get_children():
			child.queue_free()
		get_node("player/KinematicBody2D").freeze()
		get_node("god/god_animations").play("hurt_loop")
		get_node("boss_player").play("victory")
	
func update_player_bar():
	update_health_bar(get_node("hp_bars/pbar"), player_hp, PLAYER_MAX_HP, PLAYER_BAR_SECTIONS)
	
func update_god_bar():
	update_health_bar(get_node("hp_bars/gbar"), god_hp, GOD_MAX_HP, GOD_BAR_SECTIONS)
	
func update_health_bar(pbar, curr_hp, max_hp, bar_sections):
	var hp_increments = floor(max_hp / bar_sections)
	var sections_filled = floor(curr_hp / hp_increments)
	
	for child in pbar.get_children():
		if child.get_name() == "animate":
			continue
		child.set_hidden(false)
	
	if sections_filled < bar_sections:
		pbar.get_node("end").set_hidden(true)
		
	if sections_filled == 0:
		pbar.get_node("start").set_hidden(true)
		pbar.get_node("middle").set_hidden(true)
	else:
		pbar.get_node("middle").set_scale(Vector2(max(0, sections_filled-2), 1))

func _on_pit_o_doom_enter_screen():
	player_died()
	
func player_died():
	get_node("boss_move").stop_all()
	get_node("player").set_hidden(true)
	get_node("god").set_global_pos(god_init_pos)
	get_node("player/KinematicBody2D").set_fixed_process(false)
	get_node("player/KinematicBody2D/Camera2D").set_zoom(Vector2(0.25, 0.25))
	get_node("boss_player").play("game_over")
	
func reset_game():
	get_tree().set_pause(false)
	god_phase = 0
	player_hp = PLAYER_MAX_HP
	god_hp = GOD_MAX_HP
	victory = false
	
	var player_node = get_node("player")
	player_node.get_node("KinematicBody2D").set_global_pos(player_init_pos)
	player_node.get_node("KinematicBody2D/Camera2D").set_zoom(Vector2(0.25, 0.25))
	player_node.get_node("KinematicBody2D/Camera2D").set_offset(Vector2(-5, 0))
	
	var god_node = get_node("god")
	god_node.set_global_pos(god_init_pos)
	god_node.get_node("KinematicBody2D").set_phase(0)
	
	for l in ['A', 'B', 'C', 'D']:
		get_node("music/theClimb" + l).play()
	
	get_node("boss_player").stop_all()
	get_node("hp_bars/pbar").set_hidden(true)
	get_node("hp_bars/gbar").set_hidden(true)
	get_node("hp_bars/godballs_logo").set_opacity(0)
	get_node("hp_bars/victory_fade").set_opacity(0)
	get_node("hp_bars/game_over_fade").set_opacity(0)
	get_node("sunrise_player").play("init")
	
	player_node.get_node("KinematicBody2D").unfreeze()

func game_over_respawn():
	print('game_over_respawn')
	get_node("player").set_hidden(false)
	get_node("player/KinematicBody2D").set_global_pos(player_restart_pos)
	get_node("player/KinematicBody2D").set_fixed_process(true)
	get_node("player/KinematicBody2D").emit_signal("pos_update", player_restart_pos)
	
func restart_boss():
	print('restart_boss')
	player_hp = PLAYER_MAX_HP
	god_hp = GOD_MAX_HP
	god_phase = 0
	get_node("god/KinematicBody2D").set_phase(0)
	get_node("god/god_animations").play("idle0")
	get_node("boss_player").play("boss_init")

func _on_boss_trigger_enter_screen():
	get_node("boss_player").play("boss_init")
	
func boss_intro_start():
	get_node("player/KinematicBody2D").freeze()
	get_node("player/KinematicBody2D/AnimatedSprite").set_flip_h(false)
	get_node("player/KinematicBody2D/camera_player").play("rumble")
	get_node("boss_sound").play("god_rumble")
	
func boss_intro_end():
	get_node("player/KinematicBody2D/camera_player").stop(true)
	get_node("boss_sound").stop_all()
	get_node("boss_player").play("boss_phase1")
	
func boss_intro_speak():
	get_node("boss_sound").play("godballs_slow")
	
func boss_intro_zoom():
	get_node("player/KinematicBody2D/camera_player").play("zoom_out")
	update_player_bar()
	update_god_bar()
	get_node("hp_bars/pbar").set_hidden(false)
	get_node("hp_bars/gbar").set_hidden(false)
	get_node("music/theSummit").set_volume(1)
	get_node("music/theSummit").play()
	get_node("player/KinematicBody2D").unfreeze()
	for l in ['A', 'B', 'C', 'D']:
		get_node("music/theClimb" + l).stop()

func boss_fire_beam():
	var chance = BEAM_FIRE_CHANCE_PHASE1 if god_phase == 0 else BEAM_FIRE_CHANCE_PHASE2
	if randf() < chance:
		var beam = get_node("god/KinematicBody2D").create_beam()
		get_node("beams").add_child(beam)
		
func boss_relocate():
	var new_location = randi() % 3
	if new_location == current_location:
		return
	print("move" + str(current_location) + str(new_location))
	get_node("boss_move").play("move" + str(current_location) + str(new_location))
	current_location = new_location
		
func boss_next_phase_intro():
	get_node("music/theSummit").stop()
	get_node("god/god_animations").play("hurt_loop")
	get_node("god/SamplePlayer2D").play("god_scream1")
	
func boss_next_phase_replenish():
	get_node("hp_bars/gbar/animate").play("replenish")
	
func boss_next_phase_init():
	god_phase += 1
	god_hp = GOD_MAX_HP
	get_node("boss_player").play("boss_phase2")
	get_node("god/KinematicBody2D").set_phase(god_phase)
	get_node("god/god_animations").play("idle1")
	get_node("music/theGod").play()
	
func victory_smoke():
	get_node("player/KinematicBody2D/camera_player").play("victory_slide")
	get_node("god/KinematicBody2D/smoke_animations").play("activate")
	
func victory_end():
	get_node("music/theGod").stop()
	victory = true

