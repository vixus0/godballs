extends AnimatedSprite

var attacking = false
var is_facing_left = false

func update_anim(velocity):
	if (attacking):
		return
	
	var walk_left = Input.is_action_pressed("ui_left")
	var walk_right = Input.is_action_pressed("ui_right")
	var jump = Input.is_action_pressed("ui_up")

	var run_anim = "run"
	var is_running_on_ground = false
	
	if (get_node("../ground_ray").is_colliding()):
		is_running_on_ground = true
	else:
		run_anim = "run_air"
	
	if (abs(velocity.x) < 7.0 && abs(velocity.y) < 7.0):
		is_running_on_ground = false
		play("idle")
	elif (velocity.x > 7.0):
		is_facing_left = false
		play(run_anim)
	elif (velocity.x < -7.0):
		is_facing_left = true
		play(run_anim)
	else:
		play("run_air")
		
	set_flip_h(is_facing_left)
	
	var anim_player = get_parent().get_node("AnimationPlayer")
	
	if (is_running_on_ground):
		if (not anim_player.is_playing()):
			anim_player.play("footsteps")
	else:
		anim_player.stop(true)

func start_attack(anim):
	attacking = true
	play(anim)
	get_parent().get_node("SamplePlayer").play("sword" + str(randi() % 4))
	
func stop_attack():
	attacking = false
	
func facing_left():
	return is_facing_left